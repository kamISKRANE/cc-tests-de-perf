# SnowTricks

SnowTricks is a website built for Snowboard passionates.
Visiitors can discover tricks and users interactions and they can also have private acces to contribute by creating a member account.

## Installation

1- Clone this repository on your local machine : 

``
git clone https://gitlab.com/kamISKRANE/cc-tests-de-perf
``

2- install [composer](https://getcomposer.org/doc/00-intro.md)

3 - Composer Intall / update
```
sudo composer install
sudo composer update
```

4-Start Docker
```
docker-compose up -d
```

5- Install the database
#####Before Symfony command you need to add "docker-compose exec php-fpm" for exec into the docker container.
```
docker-compose exec php-fpm php bin/console doctrine:schema:drop --force
```
```
docker-compose exec php-fpm php bin/console doctrine:schema:update --force
```

6- Load the fixtures using this command:
```
docker-compose exec php-fpm php bin/console doctrine:fixtures:load
```

7-Yarn
```
sudo yarn install 
sudo yarn watch 
```



####Information
There is a Makefile, with Many interesting shortened command, (code sniffer)


## Built with

This project was built with :

Framework :  

* Symfony 4.2.8

PHP:
* PHP 7.4.4

ORM : 

* Doctrine

Testing :

* PHPUnit

Templating : 

* Twig

Dependancy Management:

* Composer

CSS & JS

* Bootstrap

* JQuery
