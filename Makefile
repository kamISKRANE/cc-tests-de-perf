run:
	docker-compose up -d

run-with-logs:
	docker-compose up

stop:
	docker-compose down

build:
	docker-compose build

run-build:
	docker-compose up -d --build

run-build-with-logs:
	docker-compose up --build

linter-php:
	docker run -ti --rm -v ${CURDIR}/src:/app sixlive/php-lint-fix php-cs-fixer fix --dry-run --diff .

linter-php-auto-fix:
	docker run -ti --rm -v ${CURDIR}/src:/app sixlive/php-lint-fix php-cs-fixer fix .

console:
	docker-compose exec php-fpm php bin/console

dsuf:
	docker-compose exec php-fpm php bin/console doctrine:schema:update --force

dsud:
	docker-compose exec php-fpm php bin/console doctrine:schema:update --dump-sql

fixtures:
	docker-compose exec php-fpm php bin/console doctrine:fixtures:load -n