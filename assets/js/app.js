/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)=
// import '../css/app.css';
const $ = require('jquery');
global.$ = global.jQuery = $;
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbootstrap/css/mdb.min.css';
import '../css/style.css';
import '../css/mainstyle.css';



// Need jQuery? Install it with "yarn add jquery" , then uncomment to import it.
import 'bootstrap';
import popper from 'popper.js';
global.popper = global.Popper = popper;
import 'mdbootstrap/css/mdb.min.css';
import 'noty';
import '../js/mainJs';
import '../js/new_collection_widget';